@extends('layouts.master')

@section('judul')
    Profile
@endsection

@section('content')
    <div class="row">
        <div class="col-6 d-flex align-items-stretch flex-column">
            <div class="card bg-light d-flex flex-fill">
                <div class="card-header text-muted border-bottom-0">
                    User
                </div>
                <div class="card-body pt-0">
                    <div class="row">
                        <div class="col-7">
                            <h2 class="lead"><b>{{ $user->name }}</b>
                                @if (!empty($profile->age))
                                    {{ $profile->age }}
                                @endif
                            </h2>
                            <p class="text-muted text-sm"><b>{{ $user->email }}</b></p>

                            <ul class="ml-4 mb-0 fa-ul text-muted">
                                <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span>
                                    Address :
                                    @if (!empty($profile->address))
                                        {{ $profile->address }}
                                    @endif
                                </li>
                                <li class="small"><span class="fa-li"><i class="fas fa-lg fa-user"></i></span> Bio :
                                    @if (!empty($profile->bio))
                                        {{ $profile->bio }}
                                    @endif
                                </li>
                            </ul>
                        </div>
                        <div class="col-5 text-center">
                            <img src="{{ asset('/template/dist/img/user1-128x128.jpg') }}" alt="user-avatar"
                                class="img-circle img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6 d-flex align-items-stretch flex-column">
            <div class="card bg-light d-flex flex-fill">
                <div class="card-header text-muted border-bottom-0">
                    Lengkapi Data Diri
                </div>
                <div class="card-body pt-0">
                    <div class="row">
                        <div class="col-12">
                            <form action="/update-profile" method="post">
                                @csrf
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Age</label>
                                    <div class="col-sm-9">
                                        <input type="number" name="age" class="form-control" placeholder="Age">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Bio</label>
                                    <div class="col-sm-9">
                                        <textarea name="bio" class="form-control" cols="30"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Address</label>
                                    <div class="col-sm-9">
                                        <textarea name="address" class="form-control" cols="30"></textarea>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-sm rounded-pill btn-block">Update</button>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
