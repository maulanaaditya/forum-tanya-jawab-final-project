@extends('layouts.master')

@section('judul')
    Kategori
@endsection

@section('content')
    <form action="/category" method="POST">
        @csrf
        <div class="form-group">
            <label>Nama Kategori</label>
            <input type="text" class="form-control" name="name" placeholder="Nama Kategori">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Deskripsi</label>
            <textarea name="description" class="form-control" cols="30" rows="5"></textarea>
        </div>
        @error('deskripsi')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-success btn-sm rounded-pill">Submit</button>
    </form>
@endsection
