@extends('layouts.master')

@section('judul')
    Edit Kategori
@endsection

@section('content')
    <form action="/category/{{ $category->id }}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label>Nama Kategori</label>
            <input type="text" class="form-control" name="name" value="{{ $category->name }}" placeholder="Nama Kategori">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Deskripsi</label>
            <textarea name="description" class="form-control" cols="30" rows="5">{{ $category->description }}</textarea>
        </div>
        @error('deskripsi')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-success btn-sm rounded-pill">Ubah</button>
    </form>
@endsection
