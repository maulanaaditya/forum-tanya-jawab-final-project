@extends('layouts.master')

@section('judul')
    Daftar Kategori
@endsection

@section('content')
    <a href="/category/create" class="btn btn-primary btn-sm rounded-pill"><i class="fas fa-plus"></i> Buat
        Kategori</a>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Nama</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($categories as $key=>$item)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $item->name }}</td>
                    <td>
                        <form action="/category/{{ $item->id }}" method="POST">
                            <a href="/category/{{ $item->id }}" class="btn btn-info btn-sm rounded-pill">Detail</a>
                            @csrf
                            @method('DELETE')
                            <a href="/category/{{ $item->id }}/edit"
                                class="btn btn-success btn-sm rounded-pill">Edit</a>
                            <input type="submit" class="btn btn-danger btn-sm rounded-pill" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="3" class="text-center">Tidak Ada Data Kategori</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection
