@extends('layouts.master')

@section('judul')
    Kategori {{ $category->name }}
@endsection

@section('content')
    <table>
        <tr>
            <td class="font-weight-bold">Nama Kategori</td>
            <td>:</td>
            <td>{{ $category->name }}</td>
        </tr>
        <tr>
            <td class="font-weight-bold">Deskripsi</td>
            <td>:</td>
            <td>{{ $category->description }}</td>
        </tr>
    </table>
    <h4 class="my-3 lead">Daftar Postingan : </h4>
    {{-- <div class="row">
        @forelse ($category->posts as $item)
            <div class="col-md-4">
                <div class="card">
                    <img class="card-img-top" src="{{ asset('/thumbnail-images/' . $item->thumbnail) }}"
                        alt="{{ $item->title }}">
                    <div class="card-body">
                        <h5 class="font-weight-bold" style="height: 2em">{{ $item->title }}</h5>
                        <span class="badge badge-info">{{ $item->category->nama }}</span>
                        <p class="card-text">{{ Str::limit($item->body, 60, '...') }}</p>
                        <a href="/posts/{{ $item->id }}" class="btn btn-success btn-block btn-sm mb-2">Detail</a>
                    </div>
                </div>
            </div>
        @empty
            <p class="col-md-4 font-weight-bold">Belum ada Postingan untuk Kategori ini.</p>
        @endforelse
    </div> --}}
@endsection
