@extends('layouts.master')

@section('judul')
    Edit jawaban
@endsection

@section('content')
    <form action="/answer/{{ $answer->id }}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label>Jawaban</label>
            <textarea name="content" class="form-control" rows="10">{{ $answer->content }}</textarea>
        </div>
        @error('content')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-success btn-sm rounded-pill">Edit</button>
    </form>
@endsection
