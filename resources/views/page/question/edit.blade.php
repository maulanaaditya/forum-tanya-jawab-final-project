@extends('layouts.master')

@section('judul')
    Ubah Pertanyaan
@endsection

@section('content')
    <form action="/question/{{ $question->id }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="form-group">
            <label>Subject</label>
            <input type="text" value="{{ $question->subject }}" class="form-control" name="subject" placeholder="Subjek">
        </div>

        <div class="form-group">
            <label>Isi Pertanyaan</label>
            <textarea name="content" class="form-control" cols="30" rows="5">{{ $question->content }}</textarea>
        </div>

        <div class="form-group">
            <label>Gambar</label>
            <input type="file" class="form-control" name="image">
        </div>

        <div class="form-group">
            <label>Kategori</label>
            <select name="category_id" class="form-control">
                <option value="">-- Pilih Kategori --</option>
                @forelse ($categories as $item)
                    <option value="{{ $item->id }}" {{ $item->id === $question->category_id ? 'selected' : '' }}>
                        {{ $item->name }}</option>
                @empty
                    <option value="">-- Tidak ada Data Kategori --</option>
                @endforelse
            </select>
        </div>

        <button type="submit" class="btn btn-success btn-sm rounded-pill">Kirim</button>
    </form>
@endsection
