@extends('layouts.master')

@section('judul')
    Detail Berita
@endsection

@section('content')
    <a href="/question" class="btn btn-primary btn-sm mb-3">Kembali</a>


    <img class="card-img-top" src="{{ asset('/questions/' . $question->image) }}" alt="{{ $question->title }}">
    <p>{{ $question->created_at }}</p>

    <h3 class="font-weight-bold">{{ $question->subject }}</h3>
    <p class="card-text">{{ $question->content }}</p>
    <hr>
    <h4>{{ $answers->count() }} Jawaban</h4>
    <div class="card-footer card-comments">

        @forelse ($answers as $item)
            <div class="card-comment">
                <!-- User image -->
                <img class="img-circle img-sm" src="{{ asset('/template/dist/img/user3-128x128.jpg') }}" alt="User Image">

                <div class="comment-text">

                    <span class="username">
                        {{ $item->user->name }}
                        <span class="text-muted float-right">{{ $item->created_at }}</span>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle dropdown-icon"
                                data-toggle="dropdown" aria-expanded="false">
                                <span class="sr-only">Opsi</span>
                            </button>
                            <div class="dropdown-menu" role="menu" style="">
                                <form action="/delete-answer/{{ $item->id }}" method="post">
                                    <a class="dropdown-item" href="/edit-answer/{{ $item->id }}">Edit</a>
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="dropdown-item">Hapus</button>
                                </form>
                            </div>
                        </div>
                    </span><!-- /.username -->
                    {{ $item->content }}

                </div>
                <!-- /.comment-text -->
            </div>
        @empty
            <p>Belum ada Jawaban</p>
        @endforelse

    </div>
    <div class="card-footer">
        <form action="/answer" method="post">
            @csrf
            <img class="img-fluid img-circle img-sm" src="{{ asset('/template/dist/img/user4-128x128.jpg') }}"
                alt="Alt Text">
            <!-- .img-push is used to add margin to elements next to floating images -->
            <div class="img-push">
                <input type="hidden" name="question_id" value="{{ $question->id }}">
                <textarea name="content" class="form-control"></textarea>
                <button type="submit" class="btn btn-success btn-sm rounded-pill mt-2">Answer</button>
            </div>
        </form>
    </div>
@endsection
