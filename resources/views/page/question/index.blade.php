@extends('layouts.master')

@section('judul')
    Kumpulan Pertanyaan
@endsection

@section('content')
    @auth
        <a href="/question/create" class="btn btn-primary btn-sm mb-3 rounded-pill"><i class="fas fa-plus"></i> Buat pertanyaan apa
            saja</a>
    @endauth
    <div class="row">
        @forelse ($questions as $item)
            <div class="col-12">
                <div class="card card-widget">
                    <div class="card-header">
                        <div class="d-flex flex-column">
                            <span class="username font-weight-bold">{{ $item->user->name }}</span>
                            <span class="description">{{ $item->created_at }}</span>
                        </div>
                        <!-- /.user-block -->
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <img class="img-fluid pad" src="{{ asset('/questions/' . $item->image) }}"
                            alt="{{ $item->subject }}">

                        <h4 class="my-3 font-weight-bold">{{ $item->subject }}</h4>
                        <p class="card-text">{{ Str::limit($item->content, 60, '...') }}</p>


                        {{-- <span class="float-right text-muted">127 likes - 3 comments</span> --}}
                        <a href="/question/{{ $item->id }}"
                            class="btn btn-info btn-block btn-sm mb-2 rounded-pill">Detail</a>
                        @auth
                            @if ($item->user->id == Auth::user()->id)
                                <div class="row">
                                    <div class="col">
                                        <a href="/question/{{ $item->id }}/edit"
                                            class="btn btn-success btn-block btn-sm rounded-pill">Edit</a>
                                    </div>
                                    <div class="col">
                                        <form action="/question/{{ $item->id }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <input type="submit" value="Delete"
                                                class="btn btn-danger btn-block btn-sm rounded-pill" data-confirm-delete="true">
                                        </form>
                                    </div>
                                </div>
                            @endif
                        @endauth

                    </div>

                </div>
            </div>
        @empty
            <p>Tidak ada Pertanyaan</p>
        @endforelse
    </div>
@endsection
