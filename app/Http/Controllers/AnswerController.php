<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AnswerController extends Controller
{
    public function store(Request $request) 
    {
        $answer = new Answer();
        $answer->question_id = $request->question_id;
        $answer->user_id = Auth::user()->id;
        $answer->content = $request->content;

        $answer->save();

        return back();

    }

    public function edit($answer_id) {
        $answer = Answer::findOrFail($answer_id);
        return view('page.answer.edit', compact('answer'));

    }

    public function update(Request $request, $answer_id) {
        $request->validate([
            'content' => 'required'
        ]);
    
        $answer = Answer::findOrFail($answer_id);

        $answer->content = $request['content'];

        $answer->save();

        return redirect()->route('question.show', ['question' => $answer->question_id]);
    
    }

    public function destroy($answer_id) {
        $answer = Answer::findOrFail($answer_id);

        $answer->delete();
        return back();

    }
}
