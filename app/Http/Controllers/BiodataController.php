<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BiodataController extends Controller
{
    public function biodata()
    {
        return view ('page.daftar');
    }

    public function home(Request $request)
    {
        $namaDepan = $request->input('fname');
        $namaBelakang = $request->input('lname');

        return view ('page.home',["namaDepan" => $namaDepan,"namaBelakang" => $namaBelakang]);
    }

    public function myProfile()
    {
        $user = Auth::user();
        $profile = Profile::where('user_id', $user->id)->first();
        return view('page.profile.index', compact('user', 'profile'));
    }

    public function updateProfile(Request $request) 
    {
        $profile = new Profile();
        
        $profile->age = $request->age;
        $profile->bio = $request->bio;
        $profile->address = $request->address;
        $profile->user_id = Auth::user()->id;

        $profile->save();

        return redirect()->back();
    }
}
