<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Category;
use App\Models\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class QuestionController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'checkPermission'])->only(['edit']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::all();
        return view('page.question.index', compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('page.question.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'subject' => 'required',    
            'content' => 'required',    
            'image' => 'required|image|mimes:png,jpg,jpeg',
            'category_id' => 'required',    
        ]);

        $fileName = time().".".$request->image->extension();

        $request->image->move(public_path('questions'), $fileName);

        $question = new Question();
 
        $question->subject = $request->subject;
        $question->content = $request->content;
        $question->image = $fileName;
        $question->category_id = $request->category_id;
        $question->user_id = Auth::user()->id;
 
        $question->save();

        return redirect('/question');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $question = Question::find($id);
        $answers = Answer::where('question_id', $id)->get();
        return view('page.question.show', compact('question', 'answers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question = Question::find($id);
        $categories = Category::all();
        return view('page.question.edit', compact('question', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'subject' => 'required',    
            'content' => 'required',    
            'image' => 'image|mimes:png,jpg,jpeg',
            'category_id' => 'required'    
        ]);

        $question = Question::find($id);

        if ($request->has('image')) {
            $path = 'questions/';
            // Delete existing image
            File::delete($path,$question->image);

            // Save new Image
            $fileName = time().".".$request->image->extension();
            $request->image->move(public_path('questions'), $fileName);

            $question->image = $fileName;
            $question->save();

        }
 
        $question->subject = $request['subject'];
        $question->content = $request['content'];
        $question->category_id = $request['category_id'];
        $question->save();

        return redirect('/question');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = Question::find($id);

        // Delete existing image
        $path = 'questions/';
        File::delete($path,$question->image);

        $question->delete();
        return redirect('/question');
    }
}
