<?php

namespace App\Http\Middleware;

use App\Models\Question;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        // Get Current User
        $currentUser = Auth::user();
        // Get Question Based Request
        $questionRequest = Question::findOrFail($request->question);
        
        if ($currentUser->id !== $questionRequest->user_id) {
            return redirect()->back();
        }

        return $next($request);
    }
}
