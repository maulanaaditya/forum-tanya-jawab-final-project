<?php

use App\Http\Controllers\AnswerController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DasboardController;
use App\Http\Controllers\BiodataController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\QuestionController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[DasboardController::class, 'utama']);
Route::get('/daftar',[BiodataController::class, 'biodata']);


Route::get('/data-table', function(){
    return view('page.data-table');
});
// Profile
Route::POST('/home', [BiodataController::class,'home']);
Route::get('/myprofile', [BiodataController::class, 'myProfile']);
Route::post('/update-profile', [BiodataController::class, 'updateProfile']);

// Category
Route::resource('/category', CategoryController::class);

// Question
Route::resource('/question', QuestionController::class);

// Answer
Route::post('/answer', [AnswerController::class, 'store']);
Route::delete('/delete-answer/{answer_id}', [AnswerController::class, 'destroy']);
Route::get('/edit-answer/{answer_id}', [AnswerController::class, 'edit']);
Route::put('/answer/{answer_id}', [AnswerController::class, 'update']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
